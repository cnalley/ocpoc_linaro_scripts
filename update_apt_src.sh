#!/bin/bash
sudo sed -i -re 's/([a-z]{2}\.)?ports.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list
sudo sed -i -re 's/([a-z]{2}\.)?ubuntu-ports/ubuntu/g' /etc/apt/sources.list
sudo apt-get -y update
sudo apt-get -y install v4l-utils
